<?php

namespace AppBundle\Controller;

use AppBundle\PortfolioBuilder\ImageBuilder;
use AppBundle\Entity\Image;
use AppBundle\PortfolioRetriever\ImageRetriever;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PortfolioController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $image = new Image();

        $form = $this->_createForm($image);

        $portfolioRetriever = $this->get('app.image_retriever');
        /**
         * @var ImageRetriever $portfolioRetriever
         */
        $photos = $portfolioRetriever->getImages();
        $isHasMore = $portfolioRetriever->getIsHasMore();
        $nextPartFirstId = $portfolioRetriever->getNextPartFirstId();

        return $this->render('portfolio/index.html.twig', [
            'form'            => $form->createView(),
            'photos'          => $photos,
            'isHasMore'       => $isHasMore,
            'nextPartFirstId' => $nextPartFirstId
        ]);
    }


    /**
     * @Route("/portfolio/photos/{firstId}", name="getPhotos")
     */
    public function actionPhotos($firstId = null)
    {
        $portfolioRetriever = $this->get('app.image_retriever');
        /**
         * @var ImageRetriever $portfolioRetriever
         */
        $photos = $portfolioRetriever->getImages($firstId);
        $isHasMore = $portfolioRetriever->getIsHasMore();
        $nextPartFirstId = $portfolioRetriever->getNextPartFirstId();

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        return $this->render('portfolio/photos_list.ajax.twig', [
            'photos' => $photos,
            'data'   => [
                'isHasMore'       => $isHasMore,
                'nextPartFirstId' => $nextPartFirstId,
            ]
        ], $response);
    }


    /**
     * @Route("/portfolio/randomPhoto", name="getRandomPhoto")
     */
    public function actionRandomPhoto()
    {

        $portfolioRetriever = $this->get('app.image_retriever');
        /**
         * @var ImageRetriever $portfolioRetriever
         */
        $image = $portfolioRetriever->getRandomImage();

        if (empty($image)) {
            return $this->json(['status' => 0]);
        }

        return $this->json([
            'status' => 1,
            'data' => [
                'url'   => $this->get('assets.packages')->getUrl('/uploads/'.$image->getFileName()),
                'label' => $image->getLabel()
            ]
        ]);

    }


    /**
     * @Route("/portfolio/create", name="create")
     */
    public function createAction(Request $request)
    {
        $image = new Image();

        $form = $this->_createForm($image);

        $emptyForm = clone $form;

        $form->handleRequest($request);

        $isUploaded = false;

        if ($form->isSubmitted() && $form->isValid()) {

            $imageBuilder = $this->get('app.image_builder');
            /**
             * @var ImageBuilder $imageBuilder
             */
            $imageBuilder->create($image);

            $form = $emptyForm;
            $isUploaded = true;
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        return $this->render('portfolio/upload_form.ajax.twig', [
            'form'       => $form->createView(),
            'isUploaded' => $isUploaded,
        ], $response);

    }


    /**
     * @param $image
     * @return \Symfony\Component\Form\FormInterface
     */
    private function _createForm(Image $image)
    {
        $form = $this->createFormBuilder($image)
            ->add('label', TextType::class)
            ->add('image', FileType::class)
            ->add('save', SubmitType::class, array('label' => 'Загрузить фотографию'))
            ->getForm();

        return $form;
    }
}
