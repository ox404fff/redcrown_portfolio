<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="image")
 */
class Image
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\Length(
     *      min = 2,
     *      max = 128,
     *      minMessage = "label must be at least {{ limit }} characters long",
     *      maxMessage = "label cannot be longer than {{ limit }} characters"
     * )
     *
     * @ORM\Column(type="string", length=128)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=256, name="image_path")
     *
     */
    private $imagePath;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", name="is_deleted", options={"default":0})
     */
    private $isDeleted = false;


    /**
     * @Assert\NotBlank(message="Please, upload the portfolio photo.")
     * @Assert\Image(
     *     minWidth = 800,
     *     maxWidth = 3200,
     *     minHeight = 600,
     *     maxHeight = 2400
     * )
     */
    protected $image;

    /**
     * Set default values
     */
    public function __construct()
    {
        $this->updatedAt = new \DateTime('now');

        if (is_null($this->createdAt)) {
            $this->createdAt = new \DateTime('now');
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Image
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }


    /**
     * Set image
     *
     * @param $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }


    /**
     * Get image
     *
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     * Set path
     *
     * @param string $path
     *
     * @return Image
     */
    public function setImagePath($path)
    {
        $this->imagePath = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }


    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }


    /**
     * Get image url
     *
     * @return string
     */
    public function getFileName()
    {
        $parsedPath = pathinfo($this->getImagePath());
        return empty($parsedPath['basename']) ? '' : $parsedPath['basename'];
    }
}
