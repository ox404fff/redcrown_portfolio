<?php

namespace AppBundle\PortfolioBuilder;

use AppBundle\Entity\Image;
use AppBundle\Uploader\UploaderLocal;
use \AppBundle\Entity\Image as EntityImage;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Service for creating file and saving information about him to database
 *
 * Class ImageBuilder
 * @package AppBundle\PortfolioBuilder
 */
class ImageBuilder
{

    /**
     * @var UploaderLocal
     */
    private $_uploader;

    /**
     * @var EntityManager
     */
    private $_entityManager;


    /**
     * ImageBuilder constructor.
     * @param UploaderLocal $uploader
     * @param EntityManager $entityManager
     */
    public function __construct(UploaderLocal $uploader, EntityManager $entityManager)
    {
        $this->_uploader = $uploader;

        $this->_entityManager = $entityManager;
    }


    /**
     * Create portfolio item
     * @param EntityImage $image
     */
    public function create(Image $image)
    {
        $path = $this->_uploader->upload($image->getImage());

        $image->setImagePath($path);

        $this->_entityManager->persist($image);

        $this->_entityManager->flush();
    }

}