<?php
namespace AppBundle\Uploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Move uploaded files to directory in local file system
 *
 * Class UploaderLocal
 * @package AppBundle\Uploader
 */
class UploaderLocal implements UploaderInterface
{

    /**
     * Path to upload files
     *
     * @var string
     */
    private $path;

    /**
     * UploaderLocal constructor.
     * @param $path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @inheritdoc
     */
    public function upload(UploadedFile $file)
    {
        $newFile = $file->move($this->path,  md5(uniqid()).'.'.$file->guessExtension());

        return $newFile->getRealPath();
    }

}