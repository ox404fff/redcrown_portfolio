<?php
namespace AppBundle\Uploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface UploaderInterface
 * @package AppBundle\Uploader
 */
interface UploaderInterface
{

    /**
     * Upload file and return path to file
     *
     * @param UploadedFile $file
     * @return string
     */
    function upload(UploadedFile $file);

}