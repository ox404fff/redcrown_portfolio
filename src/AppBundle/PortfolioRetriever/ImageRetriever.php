<?php

namespace AppBundle\PortfolioRetriever;
use AppBundle\Entity\Image;
use Doctrine\ORM\EntityRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\QueryBuilder;

/**
 * Service for fetching portfolio images from database
 *
 * Class ImageRetriever
 * @package AppBundle\PortfolioRetriever
 */
class ImageRetriever
{

    /**
     * @var EntityRepository
     */
    private $_repository;

    /**
     * Limit
     *
     * @var int
     */
    private $_limit = 12;


    /**
     * @var null
     */
    private $_isHasMore = null;

    /**
     * @var null
     */
    private $_nextPartFirstId = null;

    /**
     * ImageRetriever constructor.
     * @param Registry $doctrine
     * @param string $objectName
     */
    public function __construct($doctrine, $objectName)
    {
        $this->_repository = $doctrine->getRepository($objectName);
    }


    /**
     * Set limit
     *
     * @param $limit
     */
    public function setLimit($limit)
    {
        $this->_limit = $limit;
    }


    /**
     * Get if has more images after fetch
     *
     * @return null
     */
    public function getIsHasMore()
    {
        return $this->_isHasMore;
    }


    /**
     * First id for fetch next part photos
     *
     * @return null
     */
    public function getNextPartFirstId()
    {
        return $this->_nextPartFirstId;
    }


    /**
     * Fetch images list
     *
     * @param $fromId
     * @return Image[]
     */
    public function getImages($fromId = null)
    {
        $result = $this->_createQuery($fromId)->getResult();

        /**
         * @var Image[] $result
         */
        if (count($result) > $this->_limit) {
            $this->_isHasMore = true;
            $lastRecord = array_pop($result);
        } else {
            $this->_isHasMore = false;
            $lastRecord = end($result);
            reset($result);
        }

        $this->_nextPartFirstId = empty($lastRecord) ? 0 : $lastRecord->getId();

        return $result;
    }


    /**
     * Get random image entity
     *
     * @return Image|null
     */
    public function getRandomImage()
    {
        $maxId = (int) $this->_repository->createQueryBuilder('p')
            ->select('MAX(p.id)')->getQuery()->getSingleScalarResult();
        if (empty($maxId)) {
            return null;
        }

        return $this->_repository->find(rand(1, $maxId));
    }



    /**
     * Create query
     *
     * @param $fromId
     * @return \Doctrine\ORM\Query
     */
    private function _createQuery($fromId)
    {
        $query = $this->_repository->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
            ->setMaxResults($this->_limit + 1);
        /**
         * @var QueryBuilder $query
         */
        if (!empty($fromId)) {
            $query
                ->where('p.id <= :from_id')
                ->setParameter(':from_id', (int) $fromId);
        }

        return $query->getQuery();
    }


}